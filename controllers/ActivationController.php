<?php

/**
 * The activation controller handles the user activation.
 *
 * @package User
 * @subpackage Activation
 */
class ActivationController extends Controller
{
	/**
	 * Default layout.
	 *
	 * @access public
	 * @var string the default layout for the views.
	 */
	public $layout = '//layouts/column1';

	/**
	 * Get the controller filters.
	 *
	 * @access public
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl',
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 *
	 * @access public
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',
				'actions'=>array('activation'),
				'users' => array('*'),
			),
			array('deny',
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Activates an account and redirects the user to the login (logged out) or profile (logged in) page.
	 *
	 * @param string $active_key Activation key
	 *
	 * @access public
	 * @return void
	 */
	public function actionActivation($active_key)
	{
		$model = User::model()->findByAttributes(
			array(
				'active_key' => $active_key,
			)
		);
		if (is_null($model))
			throw new CHttpException(404, Yii::t('User', '<strong>Der Benutzer wurde schon aktiviert oder der Aktivierungscode ist falsch!</strong>'));

		if ($model->activate()) {
			Yii::app()->user->setFlash('success', Yii::t('User', '<strong>Dein Benutzerkonto wurde erfolgreich aktiviert.</strong>'));
		}

		if (Yii::app()->user->isGuest) {
			$this->redirect(array('/user/user/login'));
		} else {
			$this->redirect(array('/user/user/account'));
		}
	}
}
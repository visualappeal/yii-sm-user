<?php

class ProfileController extends Controller
{
	/**
	 * Default layout.
	 *
	 * @access public
	 * @var string the default layout for the views.
	 */
	public $layout = '//layouts/column1';

	/**
	 * Get the controller filters.
	 *
	 * @access public
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl',
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 *
	 * @access public
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',
				'actions'=>array('update'),
				'users' => array('@'),
			),
			array('deny',
				'users'=>array('*'),
			),
		);
	}

	public function actionUpdate()
	{
		$model = new UserProfileForm;

		if (isset($_POST['UserProfileForm'])) {
			$model->attributes = $_POST['UserProfileForm'];

			if ($model->validate()) {

				if ($model->save()) {
					Yii::app()->user->setFlash(
						'success',
						Yii::t(
							'User.Profile',
							'<strong>Dein Profil wurde gespeichert!</strong>'
						)
					);

					$this->redirect(array('/user/user/account'));
				} else {
					Yii::app()->user->setFlash(
						'error',
						Yii::t(
							'User.Profile',
							'<strong>Dein Profil konnte nicht gespeichert werden!</strong>'
						)
					);
				}
			}
		}

		$this->render(
			'update',
			array(
				'model' => $model,
			)
		);
	}
}
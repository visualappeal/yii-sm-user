<?php

/**
 * User controller.
 *
 * @package User
 * @subpackage User
 */
class UserController extends Controller
{
	/**
	 * Default layout.
	 *
	 * @access public
	 * @var string the default layout for the views.
	 */
	public $layout = '//layouts/column1';

	/**
	 * Get the controller filters.
	 *
	 * @access public
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 *
	 * @access public
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',
				'actions' => array('registration', 'login', 'recover', 'reset'),
				'users'  =>  array('*'),
			),
			array('allow',
				'actions' => array('account', 'logout', 'profile'),
				'users'  =>  array('@'),
			),
			array('allow',
				'actions' => array('admin', 'view', 'update'),
				'roles' => array(User::LEVEL_BOARD),
			),
			array('deny',
				'users' => array('*'),
			),
		);
	}

	/**
	 * Display the login page.
	 *
	 * @access public
	 * @return void
	 */
	public function actionLogin()
	{
		if (!Yii::app()->user->isGuest) {
			$this->redirect(array('/user/user/account'));
		}

		$this->layout = '//layouts/login';

		$model = new UserLoginForm;

		if (isset($_POST['UserLoginForm'])) {
			$model->attributes = $_POST['UserLoginForm'];

			if ($model->validate() && $model->login()) {
				Yii::app()->user->setFlash(
					'success',
					Yii::t(
						'User.Login',
						'<strong>Willkommen zurück {name}!</strong>',
						array(
							'{name}' => Yii::app()->user->name,
						)
					)
				);

				$this->redirect(array('/user/user/account'));
			}
		}

		$this->render(
			'login',
			array(
				'model' => $model,
			)
		);
	}

	/**
	 * Display the registration page.
	 *
	 * @access public
	 * @return void
	 */
	public function actionRegistration()
	{
		if (!Yii::app()->user->isGuest) {
			$this->redirect(array('/user/user/account'));
		}

		$model = new UserRegistrationForm;

		if (isset($_POST['UserRegistrationForm'])) {
			$model->attributes = $_POST['UserRegistrationForm'];

			if ($model->validate()) {
				$user = new User('register');
				$user->username = $model->username;
				$user->email = $model->email;
				$user->newPassword = $model->password;

				if ($user->save()) {
					$profile = new UserProfile;
					$profile->user_id = $user->id;

					if ($profile->save()) {
						Yii::log('A new user `' . $user->username . '` registered.', 'info', 'application.modules.user.controllers.UserController');
						Yii::app()->user->setFlash(
							'success',
							Yii::t(
								'User.Register',
								'<strong>Dein Konto wurde erstellt.</strong> Wenn du möchtest kannst du jetzt dein Profil vervollständigen.'
							)
						);

						//Login user
						$identity = new UserIdentity($model->email, $model->password);
						if ($identity->authenticate()) {
							Yii::app()->user->login($identity);
							$this->redirect(array('/user/profile/update', 'registered' => true));
						} else {
							Yii::app()->user->setFlash('error', Yii::t('User', '<strong>Du konntest nicht automatisch angemeldet werden!</strong>'));
							$this->redirect(array('/site/login'));
						}
					} else {
						Yii::log('An user profile could not be created!', 'error', 'application.modules.user.controllers.UserController');
						Yii::app()->user->setFlash(
							'error',
							Yii::t(
								'User.Register',
								'<strong>Dein Profil konnte nicht erstellt werden!</strong> Wir versuchen das Problem so schnell es geht zu beheben.'
							)
						);
					}
				} else {
					Yii::log('An account could not be created!', 'error', 'application.modules.user.controllers.UserController');
					Yii::app()->user->setFlash(
						'error',
						Yii::t(
							'User.Register',
							'<strong>Dein Konto konnte nicht erstellt werden!</strong> Vielleicht gibt es schon einen Benutzer mit deine E-Mail Adresse?'
						)
					);
				}
			}
		}

		$this->render(
			'registration',
			array(
				'model' => $model,
			)
		);
	}

	/**
	 * Display the user account page.
	 *
	 * @access public
	 * @return void
	 */
	public function actionAccount()
	{
		$user = User::model()->findByPk(Yii::app()->user->id);
		if (is_null($user))
			throw new CHttpException(404, Yii::t('User', 'Dein Konto wurde gelöscht oder ist nicht mehr aktiv!'));

		$userForm = new UserUpdateForm;
		$profileForm = new UserProfileForm;

		$active = 'profile';

		if (isset($_POST['UserUpdateForm'])) {
			$userForm->attributes = $_POST['UserUpdateForm'];

			if ($userForm->validate()) {
				$user->setNewPassword($userForm->newPassword);

				if ($user->save()) {
					$userForm->reset();

					Yii::app()->user->setFlash(
						'success',
						Yii::t(
							'User.Account',
							'<strong>Dein Benutzerkonto wurde gespeichert.</strong>'
						)
					);
				} else {
					Yii::log('An user account could not be saved!', 'error', 'application.modules.user.controllers.UserController');
					Yii::app()->user->setFlash(
						'error',
						Yii::t(
							'User.Account',
							'<strong>Dein Benutzerkonto konnte nicht gespeichert werden!</strong>'
						)
					);
				}
			}
		}

		if (isset($_POST['UserProfileForm'])) {
			$active = 'profile';

			$profileForm->attributes = $_POST['UserProfileForm'];

			if ($profileForm->validate()) {
				if ($profileForm->save()) {
					Yii::app()->user->setFlash(
						'success',
						Yii::t(
							'User.Profile',
							'<strong>Dein Profil wurde gespeichert.</strong>'
						)
					);
				} else {
					Yii::log('An user profile could not be saved!', 'error', 'application.modules.user.controllers.UserController');
					Yii::app()->user->setFlash(
						'error',
						Yii::t(
							'User.Account',
							'<strong>Dein Profil konnte nicht gespeichert werden!</strong>'
						)
					);
				}
			}
		}

		$this->render(
			'account',
			array(
				'user' => $userForm,
				'profile' => $profileForm,
				'active' => $active,
			)
		);
	}

	/**
	 * Display user data.
	 *
	 * @param integer $id User ID
	 *
	 * @access public
	 * @return void
	 */
	public function actionView($id)
	{
		$this->layout = '//layouts/admin';

		$model = User::model()->findByPk($id);
		if (is_null($model))
			throw new CHttpException(404, Yii::t('User', 'Der Benutzer konnte nicht gefunden werden.'));

		$this->render(
			'view',
			array(
				'model' => $model,
			)
		);
	}

	/**
	 * Display user profile.
	 *
	 * @param integer $id User ID
	 *
	 * @access public
	 * @return void
	 */
	public function actionProfile($id)
	{
		$model = User::model()->findByPk($id);
		if (is_null($model))
			throw new CHttpException(404, Yii::t('User', 'Der Benutzer konnte nicht gefunden werden.'));

		$this->render(
			'profile',
			array(
				'model' => $model,
			)
		);
	}

	/**
	 * Recover the user password.
	 *
	 * @access public
	 * @return void
	 */
	public function actionRecover()
	{
		$model = new UserRecoverForm;

		if (isset($_POST['UserRecoverForm'])) {
			$model->attributes = $_POST['UserRecoverForm'];

			if ($model->validate()) {
				$recover = $model->recover();
				if ($recover === true) {
					Yii::app()->user->setFlash('success', Yii::t('User.Recover', 'Du hast eine E-Mail mit den Anweisungen bekommen wie du dein Passwort zurücksetzen kannst.'));
					
					$this->redirect(array('/user/user/login'));
				} else {
					throw new CHttpException(
						500, 
						$recover
					);
				}
			}
		}

		$this->render(
			'recover',
			array(
				'model' => $model,
			)
		);
	}

	/**
	 * Reset the password.
	 *
	 * @access public
	 * @return void
	 */
	public function actionReset($code)
	{
		$model = User::model()->findByAttributes(
			array(
				'recover_key' => $code,
			)
		);

		if ($model === null)
			throw new CHttpException(400, Yii::t('User.Recover', 'Das Passwort wurde für den Benutzer schon zurück gesetzt!'));

		$model->recover_key = '';
		$password = substr(md5(time() . $model->id), 0, 8);
		$model->setNewPassword($password);

		if ($model->save()) {
			Yii::app()->user->setFlash('success', Yii::t('User.Recover', 'Dein Passwort wurde geändert. Dein neues Passwort lautet: {password}', array('{password}' => $password)));
		} else {
			Yii::app()->user->setFlash('error', Yii::t('User.Recover', 'Dein Passwort konnte nicht geändert werden! Bitte probiere es später noch einmal.'));
		}

		$this->redirect(array('/user/user/login'));
	}

	/**
	 * Updates some of the user properties.
	 *
	 * @access public
	 * @return void
	 */
	public function actionUpdate($id)
	{
		$this->layout = '//layouts/admin';
		
		$model = User::model()->findByPk($id);
		if (is_null($model))
			throw new CHttpException(404, Yii::t('User', 'Der Benutzer konnte nicht gefunden werden.'));

		if (isset($_POST['User'])) {
			$model->attributes = $_POST['User'];

			if ($model->save()) {
				Yii::app()->user->setFlash('success', Yii::t('User', '<strong>Der Benutzer wurde gespeichert.</strong>'));
				$this->redirect(array('/user/user/view', 'id' => $model->id));
			}
		}

		$this->render(
			'update',
			array(
				'model' => $model,
			)
		);
	}

	/**
	 * Manage the users.
	 *
	 * @access public
	 * @return void
	 */
	public function actionAdmin()
	{
		$this->layout = '//layouts/admin';
		$model = new User('search');
		
		$model->unsetAttributes();
		
		if (isset($_GET['User']))
			$model->attributes = $_GET['User'];
		
		$this->render(
			'/user/admin', 
			array(
				'model' => $model,
			)
		);
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 *
	 * @access public
	 * @return void
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
}
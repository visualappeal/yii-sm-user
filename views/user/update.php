<?php

$this->breadcrumbs = array(
	Yii::t('User', 'Benutzer') => array('/user/user/admin'),
	$model->name => array('/user/user/view', 'id' => $model->id),
	Yii::t('User', 'Bearbeiten'),
);

$this->beginWidget(
	'AdminWidget', 
	array(
		'title' => $model->name,
		'icon' => 'user',
	)
);

$this->renderPartial(
	'_form',
	array(
		'model' => $model,
	)
);

$this->endWidget();
<?php 
$this->pageTitle = Yii::app()->name . ' - ' . Yii::t('User.Recover', 'Passwort zurücksetzen');

$this->breadcrumbs = array(
	Yii::t('User.Recover', 'Passwort zurücksetzen'),
);
?>

<h2><?php echo Yii::t('User.Recover', 'Passwort zurücksetzen'); ?></h2>

<div class="row">
	<div class="col-sm-6">
		<?php 
		$form = $this->beginWidget('EBootstrapActiveForm', array(
			'id' => 'registration-form',
		)); 
		?>
			
			<?php echo $form->beginControlGroup($model, 'email'); ?>
				<?php echo $form->labelEx($model, 'email'); ?>
				<?php echo $form->beginControls(); ?>
					<?php echo $form->textField($model, 'email'); ?>
					<?php echo $form->error($model, 'email'); ?>
				<?php echo $form->endControls(); ?>
			<?php echo $form->endControlGroup(); ?>
			
			<?php echo $form->beginActions(); ?>
				<?php echo EBootstrap::submitButton(Yii::t('User.Recover', 'Zurücksetzen'), 'success', 'large'); ?>
			<?php echo $form->endActions(); ?>
		
		<?php $this->endWidget(); ?>
	</div>
	
	<div class="col-sm-6">
		<h3><?php echo Yii::t('User.Recover', 'Wie setze ich mein Passwort zurück?'); ?></h3>
		<p><?php echo Yii::t('User.Recover', 'Gebe deine E-Mail Adresse ein und klicke auf <em>Zurücksetzen</em>. Du bekommst dann eine E-Mail an die Adresse geschickt. Befolge die Anweisungen in der E-Mail um dein Passwort zu ändern. Falls dir dein Passwort wieder einfällt, kannst du dich auch mit deinem alten Passwort anmelden, solange du dein Passwort nicht zurück gesetzt hast natürlich.'); ?></p>
	</div>
</div>
<?php 
$this->pageTitle = Yii::app()->name . ' - ' . Yii::t('User.Register', 'Registrieren');

$this->breadcrumbs = array(
	Yii::t('User.Register', 'Registrieren'),
);

$jsFile = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('application.modules.user.js') . '/password_strength.js');
Yii::app()->clientScript->registerScriptFile($jsFile);
?>

<h2><?php echo Yii::t('User.Register', 'Registrieren'); ?></h2>

<div class="row">
	<div class="span6">
		<?php 
		$form = $this->beginWidget('EBootstrapActiveForm', array(
			'id' => 'registration-form',
		)); 
		?>
			<?php echo $form->errorSummary($model); ?>
			
			<?php echo $form->beginControlGroup($model, 'username'); ?>
				<?php echo $form->labelEx($model, 'username'); ?>
				<?php echo $form->beginControls(); ?>
					<?php echo $form->textField($model, 'username', array('class' => 'username')); ?>
					<?php echo $form->error($model, 'username'); ?>
				<?php echo $form->endControls(); ?>
			<?php echo $form->endControlGroup(); ?>
			
			<?php echo $form->beginControlGroup($model, 'email'); ?>
				<?php echo $form->labelEx($model, 'email'); ?>
				<?php echo $form->beginControls(); ?>
					<?php echo $form->textField($model, 'email'); ?>
					<?php echo $form->error($model, 'email'); ?>
				<?php echo $form->endControls(); ?>
			<?php echo $form->endControlGroup(); ?>
			
			<?php echo $form->beginControlGroup($model, 'password'); ?>
				<?php echo $form->labelEx($model, 'password'); ?>
				<?php echo $form->beginControls(); ?>
					<?php echo $form->passwordField($model, 'password', array('class' => 'password_strength')); ?>
					<?php echo $form->error($model, 'password'); ?>
				<?php echo $form->endControls(); ?>
			<?php echo $form->endControlGroup(); ?>
			
			<?php echo $form->beginControlGroup($model, 'verifyPassword'); ?>
				<?php echo $form->labelEx($model, 'verifyPassword'); ?>
				<?php echo $form->beginControls(); ?>
					<?php echo $form->passwordField($model, 'verifyPassword'); ?>
					<?php echo $form->error($model, 'verifyPassword'); ?>
				<?php echo $form->endControls(); ?>
			<?php echo $form->endControlGroup(); ?>
			
			<?php echo $form->beginActions(); ?>
				<?php echo EBootstrap::submitButton(Yii::t('User.Register', 'Registrieren'), 'success', 'large'); ?>
			<?php echo $form->endActions(); ?>
		
		<?php $this->endWidget(); ?>
	</div>
	
	<div class="span6">
		<h3><?php echo Yii::t('User.Register', 'Warum registrieren?'); ?></h3>
		<p><?php echo Yii::t('User.Register', 'Wenn du dich registriert kannst du an <strong>Projekten teilnehmen</strong>, <strong>eigene Projekte erstellen</strong> und dich mit anderen Mitarbeitern und Mitgliedern <strong>austauschen</strong>. Du bekommst außerdem exklusiven Zugriff auf den <strong>Mitglieder- und Mitarbeiterbereich</strong>.'); ?></p>
		
		<h3><?php echo Yii::t('User.Register', 'Was passiert mit meinen Daten?'); ?></h3>
		<p><?php echo Yii::t('User.Register', 'Deine Daten werden nur bei uns gespeichert. Sie werden weder verkauft noch auf irgendeine andere Art weitergegeben.'); ?></p>
	</div>
</div>
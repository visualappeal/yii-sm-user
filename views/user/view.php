<?php
if (Yii::app()->user->isAdmin()) {
	$this->breadcrumbs = array(
		Yii::t('User', 'Benutzer') => array('/user/user/admin'),
		$model->name,
	);
} else {
	$this->breadcrumbs = array(
		Yii::t('User', 'Benutzer'),
		$model->name,
	);
}
$this->beginWidget(
	'AdminWidget', 
	array(
		'title' => $model->name,
		'icon' => 'user',
		'buttons' => array(
			EBootstrap::ibutton(Yii::t('User', 'bearbeiten'), array('/user/user/update', 'id' => $model->id), '', 'mini', false, 'pencil'),
		),
		'table' => true,
	)
);

$this->widget(
	'EBootstrapDetailView', 
	array(
		'data' => $model,
		'attributes' => array(
			'name',
			'levelName',
			'statusName',
			'username',
			'email' => array(
				'name' => 'email',
				'value' => (!empty($model->email)) ? EBootstrap::link($model->email, 'mailto:' . $model->email) : '',
				'type' => 'html',
			),
			'created',
			'lastvisit',
			'theProfile.birthday' => array(
				'name' => 'theProfile.birthday',
				'value' => (!empty($model->theProfile->birthday) and $model->theProfile->birthday != '0000-00-00') ? date('d.m.Y', strtotime($model->theProfile->birthday)) : '',
			),
			'theProfile.email' => array(
				'name' => 'theProfile.email',
				'value' => (!empty($model->theProfile->email)) ? EBootstrap::link($model->theProfile->email, 'mailto:' . $model->theProfile->email) : '',
				'type' => 'html',
			),
			'theProfile.website' => array(
				'name' => 'theProfile.website',
				'value' => (!empty($model->theProfile->website)) ? EBootstrap::link($model->theProfile->website, $model->theProfile->website) : '',
				'type' => 'html',
			),
		),
	)
);

$this->endWidget();
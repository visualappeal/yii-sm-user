<?php 
$this->title[] = Yii::t('User.Account', 'Dein Benutzerkonto');

$this->breadcrumbs = array(
	Yii::t('User.Account', 'Benutzerkonto'),
);

$jsFile = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('application.modules.user.js') . '/password_strength.js');
Yii::app()->clientScript->registerScriptFile($jsFile);
?>

<h2><?php echo Yii::t('User.Account', 'Dein Benutzerkonto'); ?></h2>

<?php
$this->widget(
	'EBootstrapTabNavigation', 
	array(
		'items' => array(
			array('label' => Yii::t('User.Profile', 'Profil'), 'url' => '#profile', 'active' => ($active == 'profile')),
			array('label' => Yii::t('User.Account', 'Passwort'), 'url' => '#account', 'active' => ($active == 'user')),
		)
	)
);
?>

<?php
$this->beginWidget('EBootstrapTabContentWrapper');

    $this->beginWidget('EBootstrapTabContent', array('active' => ($active == 'profile'), 'id' => 'profile'));
		$form = $this->beginWidget('EBootstrapActiveForm', array(
			'id' => 'account-profile-form',
		));
	?>

			<div class="row">
				<div class="col-sm-6 col-md-4">
					<?php echo $form->errorSummary($profile); ?>
					
					<?php echo $form->bootstrapTextField($profile, 'firstname'); ?>
					
					<?php echo $form->bootstrapTextField($profile, 'lastname'); ?>

					<?php echo $form->bootstrapTextField($profile, 'birthday', array('class' => 'datepicker')); ?>

					<?php echo $form->bootstrapTextField($profile, 'email'); ?>

					<?php echo $form->bootstrapTextField($profile, 'website'); ?>
					
					<?php echo $form->beginActions(); ?>
						<?php echo EBootstrap::submitButton(Yii::t('User.Profile', 'Speichern'), 'success', 'large'); ?>
					<?php echo $form->endActions(); ?>
				</div>

				<div class="col-sm-6 col-md-8">
					<h3><?php echo Yii::t('User.Profile', 'Dein öffentliches Profil'); ?></h3>

					<p><?php echo Yii::t('User.Profile', 'Die Daten die du hier eingibst sind für anderen Benutzer, nicht für fremde, sichtbar. Gebe hier nur die Daten ein die jeder von dir wissen darf. Du kannst auch alle Felder leer lassen.') ?></p>
				</div>
			</div>

		<?php $this->endWidget(); ?>

	<?php $this->endWidget(); ?>

	<?php
	$this->beginWidget('EBootstrapTabContent', array('active' => ($active == 'user'), 'id' => 'account'));
	
		$form = $this->beginWidget('EBootstrapActiveForm', array(
			'id' => 'account-user-form',
		));
		?>

			<div class="row">
				<div class="col-sm-6">
					<?php echo $form->bootstrapPasswordField($user, 'newPassword', array('class' => 'password_strength')); ?>

					<?php echo $form->bootstrapPasswordField($user, 'verifyNewPassword'); ?>
					
					<?php echo $form->beginActions(); ?>
						<?php echo EBootstrap::submitButton(Yii::t('User.Account', 'Speichern'), 'success', 'large'); ?>
					<?php echo $form->endActions(); ?>
				</div>
			</div>

		<?php $this->endWidget(); ?>

	<?php $this->endWidget(); ?>

<?php $this->endWidget(); ?>
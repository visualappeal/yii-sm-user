<?php $this->pageTitle = Yii::app()->name . ' - ' . Yii::t('User.Login', "Login"); ?>

<?php 
//Placeholder
$jsFile = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('application.modules.user.js').'/modernizr.custom.js');
Yii::app()->clientScript->registerScriptFile($jsFile);
Yii::app()->clientScript->registerScript('user-login-placeholder', '
	$(function() {
		$(".control-group label").css("display", "none");
		
		if (!Modernizr.input.placeholder) {
			$("[placeholder]").focus(function() {
				var input = $(this);
				if (input.val() == input.attr("placeholder")) {
					input.val("");
					input.removeClass("placeholder");
				}
			}).blur(function() {
				var input = $(this);
				if (input.val() == "" || input.val() == input.attr("placeholder")) {
					input.addClass("placeholder");
					input.val(input.attr("placeholder"));
				}
			}).blur();
				$("[placeholder]").parents("form").submit(function() {
					$(this).find("[placeholder]").each(function() {
					var input = $(this);
					if (input.val() == input.attr("placeholder")) {
						input.val("");
					}
				});
			});
		}
	});
');
?>

<?php 
$form = $this->beginWidget(
	'EBootstrapActiveForm',
	array(
		'id' => 'user-login-form',
	)
);
?>

	<?php echo $form->beginControlGroup($model, 'username'); ?>
		<?php echo $form->label($model, 'username'); ?>
		<?php echo $form->textField($model, 'username', array('placeholder' => Yii::t('User', 'Benutzername oder E-Mail'))); ?>
		<?php echo $form->error($model, 'username'); ?>
	<?php echo $form->endControlGroup($model, 'username'); ?>
	
	<?php echo $form->beginControlGroup($model, 'password'); ?>
		<?php echo $form->label($model, 'password'); ?>
		<?php echo $form->passwordField($model, 'password', array('placeholder' => Yii::t('User', 'Passwort'))) ?>
		<?php echo EBootstrap::link(Yii::t('User.Login', 'Registrieren'), array('/user/user/registration')); ?> | <?php echo EBootstrap::link(Yii::t('User.Login', 'Passwort vergessen?'), array('/user/user/recover')); ?>
	<?php echo $form->endControlGroup($model, 'password'); ?>

	<div class="clearfix">
		<?php echo $form->checkBox($model, 'rememberMe', array('class' => 'pull-left')); ?> <?php echo $form->label($model, 'rememberMe'); ?>
	</div>
	
	<div class="clearfix">
		<div class="pull-right">
			<?php echo EBootstrap::submitButton(Yii::t('User', 'Anmelden'), 'success'); ?>	
		</div>
	</div>
	
<?php $this->endWidget(); ?>
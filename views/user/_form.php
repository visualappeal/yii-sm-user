<?php
$form = $this->beginWidget('EBootstrapActiveForm', array(
	'id' => 'project-form',
	'horizontal' => true,
)); 
?>

	<?php echo $form->beginControlGroup($model, 'level'); ?>
		<?php echo $form->labelEx($model, 'level'); ?>
		<?php echo $form->beginControls($model, 'level'); ?>
			<?php echo $form->dropDownList($model, 'level', User::getLevelDropdown()); ?>
			<?php echo $form->error($model, 'level'); ?>
		<?php echo $form->endControls($model, 'level'); ?>
	<?php echo $form->endControlGroup($model, 'level'); ?>

	<?php echo $form->beginControlGroup($model, 'status'); ?>
		<?php echo $form->labelEx($model, 'status'); ?>
		<?php echo $form->beginControls($model, 'status'); ?>
			<?php echo $form->dropDownList($model, 'status', User::getStatusDropdown()); ?>
			<?php echo $form->error($model, 'status'); ?>
		<?php echo $form->endControls($model, 'status'); ?>
	<?php echo $form->endControlGroup($model, 'status'); ?>

	<?php echo $form->beginActions(); ?>
		<?php echo EBootstrap::submitButton($model->isNewRecord ? Yii::t('User', 'Erstellen') : Yii::t('User', 'Speichern'), 'success', '', false, $model->isNewRecord ? 'plus' : 'ok', true); ?>
	<?php echo $form->endActions(); ?>

<?php 
$this->endWidget();
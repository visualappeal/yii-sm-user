<?php
$this->title[] = Yii::t('User', 'Benutzer');

$this->breadcrumbs = array(
	Yii::t('User', 'Benutzer'),
);

$this->beginWidget(
	'AdminWidget', 
	array(
		'title' => Yii::t('User', 'Benutzer'),
		'icon' => 'user',
		'table' => true,
	)
);

$this->widget(
	'EBootstrapGridView', 
	array(
		'id' => 'user-grid',
		'dataProvider' => $model->search(),
		'filter' => $model,
		'columns' => array(
			'id',
			'name_search' => array(
				'name' => 'name_search',
				'value' => '$data->name',
			),
			array(
				'class'=>'EBootstrapButtonColumn',
				'template' => '{view}{delete}',
			),
		),
		'pager' => array(
			'class' => 'EBootstrapLinkPager',
			'header' => false,
		),
		'pagerAlign' => 'right',
	)
); 

$this->endWidget();
<?php
if (Yii::app()->user->isAdmin()) {
	$this->breadcrumbs = array(
		Yii::t('User', 'Benutzer') => array('/user/user/admin'),
		$model->name,
	);
} else {
	$this->breadcrumbs = array(
		Yii::t('User', 'Benutzer'),
		$model->name,
	);
}
?>

<h2><?php echo $model->name; ?> (<?php echo $model->levelName; ?>)</h2>

<?php	
$this->widget(
	'EBootstrapDetailView', 
	array(
		'data' => $model,
		'attributes' => array(
			'name',
			'theProfile.birthday' => array(
				'name' => 'theProfile.birthday',
				'value' => (!empty($model->theProfile->birthday) and $model->theProfile->birthday != '0000-00-00') ? date('d.m.Y', strtotime($model->theProfile->birthday)) : '',
			),
			'theProfile.email' => array(
				'name' => 'theProfile.email',
				'value' => (!empty($model->theProfile->email)) ? EBootstrap::link($model->theProfile->email, 'mailto:' . $model->theProfile->email) : '',
				'type' => 'html',
			),
			'theProfile.website' => array(
				'name' => 'theProfile.website',
				'value' => (!empty($model->theProfile->website)) ? EBootstrap::link($model->theProfile->website, $model->theProfile->website) : '',
				'type' => 'html',
			),
		),
	)
);
?>

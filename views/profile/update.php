<?php
$this->pageTitle = Yii::app()->name . ' - ' . Yii::t('User.Profile', 'Profil bearbeiten');

$this->breadcrumbs = array(
	Yii::t('User', 'Dein Konto') => array('/user/user/account'),
	Yii::t('User.Profile', 'Profil bearbeiten'),
);
?>

<h2><?php echo Yii::t('User.Profile', 'Bearbeite dein Profil'); ?></h2>

<div class="row">
	<div class="span6">
		<?php
		$form = $this->beginWidget(
			'EBootstrapActiveForm',
			array(
				'id' => 'user-profile-update-form',
			)
		);
		?>

			<?php echo $form->errorSummary($model); ?>
						
			<?php echo $form->beginControlGroup($model, 'firstname'); ?>
				<?php echo $form->labelEx($model, 'firstname'); ?>
				<?php echo $form->beginControls($model, 'firstname'); ?>
					<?php echo $form->textField($model, 'firstname'); ?>
					<?php echo $form->error($model, 'firstname'); ?>
				<?php echo $form->endControls($model, 'firstname'); ?>
			<?php echo $form->endControlGroup($model, 'firstname'); ?>

			<?php echo $form->beginControlGroup($model, 'lastname'); ?>
				<?php echo $form->labelEx($model, 'lastname'); ?>
				<?php echo $form->beginControls($model, 'lastname'); ?>
					<?php echo $form->textField($model, 'lastname'); ?>
					<?php echo $form->error($model, 'lastname'); ?>
				<?php echo $form->endControls($model, 'lastname'); ?>
			<?php echo $form->endControlGroup($model, 'lastname'); ?>

			<?php echo $form->beginControlGroup($model, 'birthday'); ?>
				<?php echo $form->labelEx($model, 'birthday'); ?>
				<?php echo $form->beginControls($model, 'birthday'); ?>
					<?php echo $form->textField($model, 'birthday'); ?>
					<?php echo $form->error($model, 'birthday'); ?>
				<?php echo $form->endControls($model, 'birthday'); ?>
			<?php echo $form->endControlGroup($model, 'birthday'); ?>

			<?php echo $form->beginControlGroup($model, 'website'); ?>
				<?php echo $form->labelEx($model, 'website'); ?>
				<?php echo $form->beginControls($model, 'website'); ?>
					<?php echo $form->textField($model, 'website'); ?>
					<?php echo $form->error($model, 'website'); ?>
				<?php echo $form->endControls($model, 'website'); ?>
			<?php echo $form->endControlGroup($model, 'website'); ?>

			<?php echo $form->beginControlGroup($model, 'email'); ?>
				<?php echo $form->labelEx($model, 'email'); ?>
				<?php echo $form->beginControls($model, 'email'); ?>
					<?php echo $form->textField($model, 'email'); ?>
					<?php echo $form->error($model, 'email'); ?>
				<?php echo $form->endControls($model, 'email'); ?>
			<?php echo $form->endControlGroup($model, 'email'); ?>

			<?php echo $form->beginActions(); ?>
				<?php echo EBootstrap::submitButton(Yii::t('User.Profile', 'Profil speichern'), 'success', 'large', '', 'ok', true); ?>
			<?php echo $form->endActions(); ?>

		<?php $this->endWidget(); ?>
	</div>

	<div class="span6">
		<h3><?php echo Yii::t('User.Profile', 'Öffentliches Profil'); ?></h3>

		<p>
			<?php echo Yii::t('User.Profile', 'Dein Profil ist für alle angemeldeten Benutzer sichtbar. Trage nur Daten ein von denen du willst das sie bekannt werden können.'); ?>
		</p>
	</div>
</div>
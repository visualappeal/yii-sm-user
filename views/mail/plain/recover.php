<?php $this->beginContent('//layouts/plainmail'); ?>

<?php echo Yii::t('User.Recover', 'Möchtest du dein Passwort zurücksetzen'); ?>


<?php echo Yii::t('User.Recover', 'Jemand wollte dein Passwort zurücksetzen. Klicke auf den unten stehenden Link um dein Passwort zurückzusetzen. Wenn du das nicht warst, kannst du die E-Mail einfach ignorieren.'); ?>


<?php echo $url; ?>

<?php $this->endContent();
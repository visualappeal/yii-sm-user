<?php $this->beginContent('//layouts/htmlmail'); ?>

<h1><?php echo Yii::t('User.Recover', 'Möchtest du dein Passwort zurücksetzen?'); ?></h1>

<p><?php echo Yii::t('User.Recover', 'Jemand wollte dein Passwort zurücksetzen. Klicke auf den <a href="{url}">Link</a> um dein Passwort zurückzusetzen.', array(
	'{url}' => $url,
)); ?></p>

<?php 
$this->widget('EmailButtonWidget', array(
	'title' => Yii::t('User.Recover', 'Zurücksetzen'),
	'link' => $url,
));
?>

<p>
	<?php echo Yii::t('User.Recover', 'Falls du dein Passwort nicht zurückgesetzt hast, kannst du die E-Mail einfach ignorieren.') ?>
</p>

<?php $this->endContent();
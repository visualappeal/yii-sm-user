<?php

/**
 * User Module.
 */
class UserModule extends CWebModule
{
	/**
	 * Init application.
	 *
	 * @access protected
	 * @return void
	 */
	protected function init()
	{
		Yii::import('application.modules.user.models.*');
		Yii::import('application.modules.user.components.*');

		return parent::init();
	}

	/**
	 * Get all members.
	 *
	 * @access public
	 * @return array Array of User
	 */
	public function getMember() {
		return User::model()->member()->findAll(); 
	}

	/**
	 * Get all users in the staff.
	 *
	 * @access public
	 * @return array Array of User
	 */
	public function getStaff() {
		return User::model()->staff()->findAll(); 
	}

	/**
	 * Get all users in the board.
	 *
	 * @access public
	 * @return array Array of User
	 */
	public function getBoard() {
		return User::model()->board()->findAll(); 
	}
}
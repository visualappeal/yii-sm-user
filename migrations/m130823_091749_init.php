<?php

/**
 * Erste Migration um notwendige Tabellen zu erstellen.
 */
class m130823_091749_init extends CDbMigration
{
	public function safeUp()
	{
		$this->createTable(
			'{{user}}',
			array(
				'id' => 'int(10) unsigned NOT NULL AUTO_INCREMENT',
				'username' => 'varchar(20) NOT NULL',
				'password' => 'varchar(80) NOT NULL',
				'email' => 'varchar(150) NOT NULL',
				'active_key' => 'varchar(32) NOT NULL',
				'recover_key' => 'varchar(32) NOT NULL',
				'created' => 'datetime NOT NULL',
				'lastvisit' => 'datetime NOT NULL',
				'level' => 'tinyint(3) unsigned NOT NULL',
				'status' => 'tinyint(3) unsigned NOT NULL',
				'PRIMARY KEY (`id`)',
				'UNIQUE KEY `username` (`username`)',
				'UNIQUE KEY `email` (`email`)',
			)
		);

		$this->createTable(
			'{{user_profile}}',
			array(
				'id' => 'int(10) unsigned NOT NULL AUTO_INCREMENT',
				'user_id' => 'int(10) unsigned NOT NULL',
				'firstname' => 'varchar(50) NOT NULL',
				'lastname' => 'varchar(100) NOT NULL',
				'birthday' => 'date NOT NULL',
				'website' => 'varchar(100) NOT NULL',
				'email' => 'varchar(100) NOT NULL',
				'PRIMARY KEY (`id`)',
				'KEY `user_id` (`user_id`)',
			)
		);
	}

	public function safeDown()
	{
		$this->dropTable('{{user_profile}}');
		$this->dropTable('{{user}}');
	}
}
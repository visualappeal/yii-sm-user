<?php

class UserProfileForm extends CFormModel
{
	/**
	 * Firstname.
	 *
	 * @access public
	 * @var string
	 */
	public $firstname;

	/**
	 * Lastname.
	 *
	 * @access public
	 * @var string
	 */
	public $lastname;

	/**
	 * Birthday.
	 *
	 * @access public
	 * @var string
	 */
	public $birthday;

	/**
	 * Website.
	 *
	 * @access public
	 * @var boolean
	 */
	public $website;

	/**
	 * Email address.
	 *
	 * @access public
	 * @var boolean
	 */
	public $email;

	/**
	 * User profile cache.
	 *
	 * @access private
	 * @var UserProfile
	 */
	private $_profile;

	/**
	 * Get the validation rules.
	 *
	 * @access public
	 * @return array
	 */
	public function rules()
	{
		return array(
			array('firstname', 'length', 'max' => 50),
			array('lastname, website, email', 'length', 'max' => 100),
			array('email', 'email'),
			array('website', 'url'),
			array('birthday', 'date', 'format' => 'd.M.yyyy', 'allowEmpty' => true, 'message' => Yii::t('User.Profile', 'Der Geburtstag muss in dem Format dd.mm.yyyy (z.B. 15.2.1991) angegeben werden.')),
		);
	}

	/**
	 * After validate method. Convert date formats.
	 *
	 * @access public
	 * @return void
	 */
	public function afterValidate()
	{
		if (parent::beforeValidate()) {
			if (!empty($this->birthday) and strtotime($this->birthday) !== false)
				$this->birthday = date('Y-m-d', strtotime($this->birthday));
		}

		return parent::afterValidate();
	}

	/**
	 * After construct method. Get user profile model.
	 *
	 * @access public
	 * @return void
	 */
	public function afterConstruct()
	{
		$profile = UserProfile::model()->findByAttributes(
			array(
				'user_id' => Yii::app()->user->id,
			)
		);

		$profile->scenario = 'update';
		$this->_profile = $profile;
		$this->attributes = $profile->attributes;

		if (!empty($this->birthday) and strtotime($this->birthday) !== false)
			$this->birthday = date('d.m.Y', strtotime($this->birthday));

		return parent::afterConstruct();
	}

	/**
	 * Save user profile with model data.
	 *
	 * @access public
	 * @return boolean
	 */
	public function save()
	{
		$this->_profile->attributes = $this->attributes;

		$success = $this->_profile->save();

		if ($success && !empty($this->birthday))
			$this->birthday = date('d.m.Y', strtotime($this->birthday));
		else
			$this->birthday = '';

		return $success;
	}

	/**
	 * Get the attribute labels.
	 *
	 * @access public
	 * @return array
	 */
	public function attributeLabels()
	{
		return array(
			'firstname' => Yii::t('User', 'Vorname'),
			'lastname' => Yii::t('User', 'Nachname'),
			'birthday' => Yii::t('User', 'Geburtstag'),
			'website' => Yii::t('User', 'Internetseite'),
			'email' => Yii::t('User', 'E-Mail Adresse'),
		);
	}
}
<?php

/**
 * User model.
 *
 * The followings are the available columns in table 'users':
 *
 * @property integer $id User ID
 * @property string $username Username
 * @property string $password Hashed and salted password
 * @property string $email Email address
 * @property string $active_key Activation key
 * @property integer $created Date of registration
 * @property integer $lastvisit Date of the last visit
 * @property integer $level User level
 * @property integer $status User status
 *
 * @package User
 */
class User extends CActiveRecord
{
	/**
	 * User level
	 */
	const LEVEL_USER = 0; //Benutzer
	const LEVEL_MEMBER = 3; //Mitglied
	const LEVEL_STAFF = 5; //Mitarbeiter
	const LEVEL_BOARD = 10; //Vorstand
	const LEVEL_EXECUTIVE_BOARD = 13; //Geschäftsführender Vorstand
	const LEVEL_ADMIN = 15; //Admin

	/**
	 * User states.
	 */
	const STATUS_ACTIVE = 0;
	const STATUS_NOTACTIVE = 1;
	const STATUS_BANED = 2;

	/**
	 * Salt for crypt user passwords
	 *
	 * @access public
	 * @var string
	 */
	public $salt = '$6$rounds=10000$hAwwWd55nad2a4fc$';

	/**
	 * Name for the admin search filter.
	 *
	 * @access public
	 * @var string
	 */
	public $name_search;

	/**
	 * Returns the static model of the specified AR class.
	 *
	 * @access public
	 * @return CActiveRecord the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * Get the model table name.
	 *
	 * @access public
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{user}}';
	}

	/**
	 * Get the validation rules.
	 *
	 * @access public
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('username, email, password', 'required'),
			array('id, level, status', 'numerical', 'integerOnly' => true),
			array('username, email', 'unique'),
			array('username', 'length', 'max' => 20, 'min' => 3),
			array('email', 'length', 'max' => 150),
			array('email', 'email'),
			array('password', 'length', 'max' => 80, 'min' => 6),

			//Search
			array('id, name_search', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * Get the user relations.
	 *
	 * @access public
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'theProfile'=>array(self::HAS_ONE, 'UserProfile', 'user_id'),
		);
	}

	/**
	 * Get the attribute labels
	 *
	 * @access public
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('User', 'ID'),
			'name' => Yii::t('User', 'Name'),
			'name_search' => Yii::t('User', 'Name'),
			'username'=>Yii::t('User', 'Benutzername'),
			'password'=>Yii::t('User', 'Passwort'),
			'verifyPassword'=>Yii::t('User', 'Passwort wiederholen'),
			'email' => Yii::t('User', 'E-Mail'),
			'active_key' => Yii::t('User', 'Aktivierungsschlüssel'),
			'created' => Yii::t('User', 'Registriert'),
			'lastvisit' => Yii::t('User', 'Letzter Besuch'),
			'level' => Yii::t('User', 'Rechte'),
			'levelName' => Yii::t('User', 'Rechte'),
			'status' => Yii::t('User', 'Status'),
			'statusName' => Yii::t('User', 'Status'),
		);
	}

	/**
	 * After find method. Convert dates to a local format.
	 *
	 * @access public
	 * @return void
	 */
	public function afterFind()
	{
		$this->created = date('d.m.Y H:i:s', strtotime($this->created));

		if ($this->lastvisit == '0000-00-00 00:00:00') {
			$this->lastvisit = '';
		} elseif (strtotime($this->lastvisit) !== false) {
			$this->lastvisit = date('d.m.Y', strtotime($this->lastvisit));
		}
	}

	/**
	 * Validates the user model before validation.
	 *
	 * @access public
	 * @return boolean
	 */
	public function beforeValidate()
	{
		if (parent::beforeValidate()) {
			if (!empty($this->created))
				$this->created = date('Y-m-d H:i:s', strtotime($this->created));
			if (!empty($this->lastvisit))
				$this->lastvisit = date('Y-m-d H:i:s', strtotime($this->lastvisit));
		}

		return parent::beforeValidate();
	}

	/**
	 * Before save method. Set creation date and send activation key on registration.
	 *
	 * @access public
	 * @return boolean
	 */
	public function beforeSave()
	{
		if (parent::beforeSave()) {
			if ($this->scenario == 'register') {
				$this->created = date('Y-m-d H:i:s');
				$this->active_key = md5(time() . $this->username);
				$url = Yii::app()->createAbsoluteUrl('/user/activation/activation', array('active_key' => $this->active_key));

				$html = '<h2>' . Yii::t('User.RegisterEmail', 'Willkommen auf der Internetseite des CVJM Langenberg') . '</h2>';
				$html .= '<p>' . Yii::t(
					'User.RegisterEmail', 
					'Um deine Registrierung abzuschließen musst du deine E-Mail Adresse {urlStart}bestätigen{urlEnd}.',
					array(
						'{urlStart}' => '<a href="' . $url . '">',
						'{urlEnd}' => '</a>',
					)
				) . '</p>';

				$plain = Yii::t('User.RegisterEmail', 'Willkommen auf der Internetseite des CVJM Langenberg') . "\n\n";
				$plain .= Yii::t(
					'User.RegisterEmail', 
					'Um deine Registrierung abzuschließen musst du deine E-Mail Adresse bestätigen:'
				) . "\n";
				$plain .= $url;

				if (!Email::send(
						array($this->email => $this->name),
						Yii::t('User.RegisterEmail', 'Willkommen'),
						$html,
						$plain
					)
				) {
					Yii::app()->user->setFlash(
						'error', 
						Yii::t(
							'User.RegisterEmail', 
							'<strong>Die Aktivierungsemail konnte nicht gesendet werden!</strong> Bitte wende dich an den {{support}}.',
							array(
								'{{support}}' => EBootstrap::link(Yii::t('Core', 'Support'), array('/site/contact')),
							)
						)
					);
				}
			}
		}

		return parent::beforeSave();
	}

	/**
	 * Activates the account.
	 *
	 * @access public
	 * @return boolean Whether the activation was successfully saved
	 */
	public function activate()
	{
		$this->active_key = '';
		return $this->save();
	}

	/**
	 * Generate Salt with CRYPT_BLOWFISH
	 *
	 * @access private
	 * @return string Salt
	 */
	private function _generateSalt() {
		$chars = './abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
		$charCount = strlen($chars);
		$salt = '$2a$10$';
		
		for ($i = 0; $i < 20; $i++) {
			$salt .= substr($chars, rand(0, $charCount-1), 1);
		}
		
		$salt .= '$';
		
		return $salt;
	}
	
	/**
	 * Encrypt password with salts.
	 *
	 * @param string $password
	 * @param string $salt
	 *
	 * @access private
	 * @return string Encrypted password
	 */
	private function _encryptPassword($password, $salt) {
		return crypt(crypt($password, $salt), $this->_generateSalt());
	}
	
	/**
	 * Get the random salt from the hash.
	 *
	 * @param string $hash
	 *
	 * @access private
	 * @return string Salt
	 */
	private function _decryptSalt($hash) {
		return substr($hash, 0, 28);
	}
	
	/**
	 * Compare a password with the existing hash
	 *
	 * @param string $password1 Cleartext password
	 * @param string salt Static salt
	 *
	 * @access public
	 * @return boolean Match
	 */
	public function passwordCompare($password1) {
		return (crypt(crypt($password1, $this->salt), $this->_decryptSalt($this->password)) == $this->password);
	}
	
	/**
	 * Set a new password for this user.
	 *
	 * @access public
	 * @return void
	 */
	public function setNewPassword($password)
	{
		$this->password = $this->_encryptPassword($password, $this->salt);
	}
	
	/**
	 * Get the name of the user.
	 *
	 * @access public
	 * @return string
	 */
	public function getName() {
		if (isset($this->theProfile)) {
			if (!empty($this->theProfile->firstname) and !empty($this->theProfile->lastname))
				return $this->theProfile->firstname . ' ' . $this->theProfile->lastname;
			if (!empty($this->theProfile->firstname))
				return $this->theProfile->firstname;
			elseif (!empty($this->theProfile->lastname))
				return $this->theProfile->lastname;
			else
				return $this->username;
		} else {
			return $this->username;
		}
	}
	
	/**
	 * Get the level alias.
	 *
	 * @access public
	 * @return string
	 */
	public function getLevelName() {
		switch ($this->level) {
			case self::LEVEL_USER: return Yii::t('User', 'Keine Rechte');
			case self::LEVEL_MEMBER: return Yii::t('User', 'Mitglied');
			case self::LEVEL_STAFF: return Yii::t('User', 'Mitarbeiter');
			case self::LEVEL_BOARD: return Yii::t('User', 'Vorstand');
			case self::LEVEL_EXECUTIVE_BOARD: return Yii::t('User', 'Geschäftsführender Vorstand');
			case self::LEVEL_ADMIN: return Yii::t('User', 'Administrator');
		}
	}

	/**
	 * Get the status alias.
	 *
	 * @access public
	 * @return string
	 */
	public function getStatusName() {
		switch ($this->status) {
			case self::STATUS_ACTIVE: return Yii::t('User', 'Aktiv');
			case self::STATUS_NOTACTIVE: return Yii::t('User', 'Nicht aktiv');
			case self::STATUS_BANED: return Yii::t('User', 'Gebannt');
		}
	}

	/**
	 * Get the level values for a dropdown list.
	 *
	 * @access public
	 * @return array
	 */
	public static function getLevelDropdown()
	{
		return array(
			self::LEVEL_USER => Yii::t('User', 'Keine Rechte'),
			self::LEVEL_MEMBER => Yii::t('User', 'Mitglied'),
			self::LEVEL_STAFF => Yii::t('User', 'Mitarbeiter'),
			self::LEVEL_BOARD => Yii::t('User', 'Vorstand'),
			self::LEVEL_EXECUTIVE_BOARD => Yii::t('User', 'Geschäftsführender Vorstand'),
			self::LEVEL_ADMIN => Yii::t('User', 'Administrator'),
		);
	}

	/**
	 * Get the status values for a dropdown list.
	 *
	 * @access public
	 * @return array
	 */
	public static function getStatusDropdown()
	{
		return array(
			self::STATUS_ACTIVE => Yii::t('User', 'Aktiv'),
			self::STATUS_NOTACTIVE => Yii::t('User', 'Nicht aktiv'),
			self::STATUS_BANED => Yii::t('User', 'Gebannt'),
		);
	}
	
	public function scopes()
    {
        return array(
            'active'=>array(
                'condition'=>'status='.self::STATUS_ACTIVE,
            ),
            'notactvie'=>array(
                'condition'=>'status='.self::STATUS_NOTACTIVE,
            ),
            'banned'=>array(
                'condition'=>'status='.self::STATUS_BANED,
            ),
            'member' => array(
            	'condition' => 'level >= :level',
            	'params' => array(
            		':level' => 3,
            	)
            ),
             'staff' => array(
            	'condition' => 'level >= :level',
            	'params' => array(
            		':level' => 5,
            	)
            ),
            'board' => array(
            	'condition' => 'level >= :level',
            	'params' => array(
            		':level' => 10,
            	)
            ),
             'executiveBoard' => array(
            	'condition' => 'level >= :level',
            	'params' => array(
            		':level' => 13,
            	)
            ),
            'level'=>array(
                'condition'=>'level >= :level',
                'params' => array(
            		':level' => 15,
            	)
            ),
            'notsafe'=>array(
            	'select' => 'id, username, password, email, active_key, created, lastvisit, level, status',
            ),
        );
    }

    /**
	 * Retrieves a list of posts based on the current search/filter conditions.
	 *
	 * @access public
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		$t = $this->getTableAlias();
		$criteria = new CDbCriteria;
		$criteria->with = array(
			'theProfile',
		);

		$criteria->compare("$t.id", $this->id);
		$criteria->compare("$t.username", $this->name_search, true, 'AND');
		$criteria->compare("CONCAT_WS(' ', theProfile.firstname, theProfile.lastname)", $this->name_search, true, 'OR');

		return new CActiveDataProvider(
			$this, 
			array(
				'criteria' => $criteria,
			)
		);
	}
}
<?php

/**
 * This is the model class for table "{{user_profile}}".
 *
 * The followings are the available columns in table '{{user_profile}}':
 * @property integer $id
 * @property integer $user_id
 * @property string $firstname
 * @property string $lastname
 * @property string $website
 * @property string $email
 */
class UserProfile extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 *
	 * @param string $className active record class name.
	 *
	 * @access public
	 * @return UserProfile the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * Get the user profile table name.
	 *
	 * @access public
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{user_profile}}';
	}

	/**
	 * Attribute validation rules.
	 *
	 * @access public
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('user_id', 'required'),
			array('user_id', 'numerical', 'integerOnly' => true),
			array('user_id', 'unsafe'),
			array('firstname', 'length', 'max' => 50),
			array('lastname, website, email', 'length', 'max' => 100),
			array('email', 'email'),
			array('website', 'url'),
			array('birthday', 'date', 'format' => 'yyyy-M-d'),
		);
	}

	/**
	 * Get user profile relations.
	 *
	 * @access public
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'theUser' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	/**
	 * After find method. Set date format
	 *
	 * @access public
	 * @return void
	 */
	public function afterFind()
	{
		if (empty($this->birthday) or ($this->birthday == '0000-00-00'))
			$this->birthday = '';

		return parent::afterFind();
	}

	/**
	 * Get the attribute labels.
	 *
	 * @access public
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('User.Profile', 'ID'),
			'user_id' => Yii::t('User.Profile', 'Benutzer'),
			'firstname' => Yii::t('User.Profile', 'Vorname'),
			'lastname' => Yii::t('User.Profile', 'Nachname'),
			'birthday' => Yii::t('User.Profile', 'Geburtstag'),
			'website' => Yii::t('User.Profile', 'Internetseite'),
			'email' => Yii::t('User.Profile', 'E-Mail Adresse'),
		);
	}
}
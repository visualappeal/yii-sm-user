<?php

class UserRecoverForm extends CFormModel
{
	/**
	 * Email address of the user.
	 *
	 * @access public
	 * @var string
	 */
	public $email;

	/**
	 * User model.
	 *
	 * @access private
	 * @var User
	 */
	private $_model = null;

	/**
	 * Get the validation rules.
	 *
	 * @access public
	 * @return array
	 */
	public function rules()
	{
		return array(
			array('email', 'length', 'max' => 150),
			array('email', 'email'),
		);
	}

	/**
	 * Check if the user exists.
	 *
	 * @access public
	 * @return boolean
	 */
	public function beforeValidate()
	{
		if (parent::beforeValidate()) {
			$this->_model = User::model()->findByAttributes(array('email' => $this->email));

			if ($this->_model === null) {
				$this->addError('email', Yii::t('User', 'Ein Benutzer mit der E-Mail Adresse konnte nicht gefunden werden!'));

				return false;
			}
		}

		return parent::beforeValidate();
	}

	/**
	 * Model attribute labels.
	 *
	 * @access public
	 * @return array
	 */
	public function attributeLabels()
	{
		return array(
			'email' => Yii::t('User.Recover', 'E-Mail Adresse'),
		);
	}

	/**
	 * Set the recover string of the user and send mail
	 *
	 * @access public
	 * @return boolean If the mail was send
	 */
	public function recover()
	{
		if ($this->_model !== null) {
			$this->_model->recover_key = md5(time() . $this->_model->id);

			if ($this->_model->save()) {
				$url = Yii::app()->createAbsoluteUrl('/user/user/reset', array('code' => $this->_model->recover_key));

				$html = Yii::app()->controller->renderPartial('/mail/html/recover', array(
					'url' => $url,
				), true);

				$plain = Yii::app()->controller->renderPartial('/mail/plain/recover', array(
					'url' => $url,
				), true);

				if (EmailQueue::queue($this->_model->email, $this->_model->name, Yii::t('User.Recover', 'Passwort zurücksetzen'), $html, $plain)) {
					return true;
				} else {
					return Yii::t(
						'User.Recover', 
						'Die E-Mail konnte leider nicht verschickt werden. Bitte versuche es später noch einmal!'
					);
				}
			} else {
				Yii::log('The recovered user could not be saved: ' . serialize($this->_model->errors), 'error', 'user.user.models.UserRecoverForm');

				return Yii::t(
					'User.Recover', 
					'Beim wiederherstellen des Passworts ist leider ein Fehler aufgetreten! Bitte versuche es später noch einmal.'
				);
			}
		}

		return Yii::t(
			'User.Recover', 
			'Ein Benutzer mit der E-Mail Adresse existiert leider nicht!'
		);
	}
}
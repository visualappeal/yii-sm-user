<?php

class UserUpdateForm extends CFormModel
{
	/**
	 * Password.
	 *
	 * @access public
	 * @var string
	 */
	public $newPassword;

	/**
	 * Passwort repetition.
	 *
	 * @access public
	 * @var string
	 */
	public $verifyNewPassword;

	/**
	 * Get the validation rules.
	 *
	 * @access public
	 * @return array
	 */
	public function rules()
	{
		return array(
			array('newPassword, verifyNewPassword', 'required'),
			array('newPassword', 'length', 'max' => 128, 'min' => 6),
			array('verifyNewPassword', 'compare', 'compareAttribute' => 'newPassword'),
		);
	}

	/**
	 * Get the attribute labels.
	 *
	 * @access public
	 * @return array
	 */
	public function attributeLabels()
	{
		return array(
			'newPassword' => Yii::t('User', 'Passwort ändern'),
			'verifyNewPassword' => Yii::t('User', 'Passwort wiederholen'),
		);
	}

	public function reset()
	{
		$this->newPassword = '';
		$this->verifyNewPassword = '';
	}
}
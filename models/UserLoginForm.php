<?php

class UserLoginForm extends CFormModel
{
	/**
	 * Username.
	 *
	 * @access public
	 * @var string
	 */
	public $username;

	/**
	 * Password.
	 *
	 * @access public
	 * @var string
	 */
	public $password;

	/**
	 * Set login cookie.
	 *
	 * @access public
	 * @var boolean
	 */
	public $rememberMe;

	/**
	 * User identity.
	 *
	 * @access private
	 * @var UserIdentity
	 */
	private $_identity;

	/**
	 * Get the validation rules.
	 *
	 * @access public
	 * @return array
	 */
	public function rules()
	{
		return array(
			array('username, password', 'required'),
			array('rememberMe', 'boolean'),
		);
	}

	/**
	 * Log in the user.
	 *
	 * @access public
	 * @return boolean
	 */
	public function login()
	{
		if (is_null($this->_identity)) {
			$this->_identity = new UserIdentity($this->username, $this->password);
			$this->_identity->authenticate();
		}
		
		if ($this->_identity->errorCode === UserIdentity::ERROR_NONE) {
			if ($this->rememberMe)
				Yii::app()->user->login($this->_identity, 2592000); //Set cookie for 30 days
			else
				Yii::app()->user->login($this->_identity);

			return true;
		} else {
			switch ($this->_identity->errorCode) {
				case UserIdentity::ERROR_USERNAME_INVALID:
					$this->addError('username', Yii::t('User.Login', 'Der Benutzername oder die E-Mail existiert nicht!'));
					break;
				case UserIdentity::ERROR_PASSWORD_INVALID:
					$this->addError('password', Yii::t('User.Login', 'Das Passwort ist falsch!'));
					break;
			}

			return false;
		}
	}

	/**
	 * Get the attribute labels.
	 *
	 * @access public
	 * @return array
	 */
	public function attributeLabels()
	{
		return array(
			'username' => Yii::t('User', 'Benutzername oder E-Mail Adresse'),
			'password' => Yii::t('User', 'Passwort'),
			'rememberMe' => Yii::t('User', 'Angemeldet bleiben'),
		);
	}
}
<?php

class UserRegistrationForm extends CFormModel
{
	/**
	 * Username.
	 *
	 * @access public
	 * @var string
	 */
	public $username;

	/**
	 * E-Mail address.
	 *
	 * @access public
	 * @var string
	 */
	public $email;

	/**
	 * Password.
	 *
	 * @access public
	 * @var string
	 */
	public $password;

	/**
	 * Passwort repetition.
	 *
	 * @access public
	 * @var string
	 */
	public $verifyPassword;

	/**
	 * Get the validation rules.
	 *
	 * @access public
	 * @return array
	 */
	public function rules()
	{
		return array(
			array('username, email, password, verifyPassword', 'required'),
			array('username', 'length', 'max' => 20, 'min' => 4),
			array('email', 'length', 'max' => 150),
			array('email', 'email'),
			array('password', 'length', 'max' => 128, 'min' => 6),
			array('verifyPassword', 'compare', 'compareAttribute' => 'password'),
		);
	}

	/**
	 * Get the attribute labels.
	 *
	 * @access public
	 * @return array
	 */
	public function attributeLabels()
	{
		return array(
			'username' => Yii::t('User', 'Benutzername'),
			'email' => Yii::t('User', 'E-Mail Adresse'),
			'password' => Yii::t('User', 'Passwort'),
			'verifyPassword' => Yii::t('User', 'Passwort wiederholen'),
		);
	}

	/**
	 * Validate registration form.
	 *
	 * @access public
	 * @return boolean
	 */
	public function bevoreValidate()
	{
		if (parent::bevoreValidate()) {
			$user = User::model()->findByAttributes(
				array(
					'username' => $this->username
				)
			);

			if (!is_null($user))
				$this->addError('username', Yii::t('User.Register', 'Es gibt schon einen Benutzer mit dem Benutzernamen!'));

			$user = User::model()->findByAttributes(
				array(
					'email' => $this->email
				)
			);

			if (!is_null($user))
				$this->addError('email', Yii::t('User.Register', 'Es gibt schon einen Benutzer mit der E-Mail Adresse!'));
		}

		return parent::bevoreValidate();
	}
}